Rails.application.routes.draw do
  
  get 'news_pages/newsPage1'

  get 'news_pages/newsPage2'

  get 'news_pages/newsPage3'

  get 'news_pages/newsPage4'

  get 'sessions/new'
  
  get 'contact', to: 'messages#contact'

  get 'message/new'

  get 'contact/new'

  get 'staff_logins/new'

  root 'static_pages#home' 
  
  get '/home', to: 'static_pages#home'

  get '/treatments', to: 'static_pages#treatments'

  get '/news', to: 'static_pages#news'

  get 'static_pages/contactUs'

  get '/aboutUs', to: 'static_pages#aboutUs'

  get '/login', to: 'staff_logins#new'
  
  post '/login',   to: 'staff_logins#create'
  
  delete '/logout',  to: 'staff_logins#destroy'
  
  get  '/signup',  to: 'staff_creations#new'
  
  post  '/signup',  to: 'staff_creations#create'


  get 'static_pages/admin'

  get 'static_pages/staff'
  
  post '/contact', to: 'messages#create'
  
  get '/contact', to: 'messages#new'
  
  post '/staff_logins/2', to: 'newspages#create'
  post '/staff_logins/2', to: 'newspages#destroy'
  post '/newsOne', to: 'newspages#create'
  
  post '/staff_logins/2', to: 'newstwopage#create'
  post '/staff_logins/2', to: 'newstwopage#destroy'
  post '/newsTwo', to: 'newstwopage#create'
  
  post '/staff_logins/2', to: 'newsthreepage#create'
  post '/staff_logins/2', to: 'newsthreepage#destroy'
  post '/newsThree', to: 'newsthreepage#create'
  
  post '/staff_logins/2', to: 'newsfourpage#create'
  post '/staff_logins/2', to: 'newsfourpage#destroy'
  post '/newsFour', to: 'newsfourpage#create'
  
  resources :staff_logins

  resources :messages
  
  resources :newspages
  
  resources :newstwopage
  resources :newsthreepage
  resources :newsfourpage


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

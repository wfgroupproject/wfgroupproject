require 'test_helper'

class MessagesControllerTest < ActionDispatch::IntegrationTest
  test "should get contact" do
    get messages_contact_url
    assert_response :success
  end

end

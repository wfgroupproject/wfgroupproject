require 'test_helper'

class NewsPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get newsPage1" do
    get news_pages_newsPage1_url
    assert_response :success
  end

  test "should get newsPage2" do
    get news_pages_newsPage2_url
    assert_response :success
  end

  test "should get newsPage3" do
    get news_pages_newsPage3_url
    assert_response :success
  end

  test "should get newsPage4" do
    get news_pages_newsPage4_url
    assert_response :success
  end

end

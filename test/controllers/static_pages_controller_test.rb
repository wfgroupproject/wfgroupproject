require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
      @base_title = "Ruby on Rails Group Project"
  end
  
  test "should get home" do
    get static_pages_home_url
    assert_response :success
    assert_select "title", "Home | #{@base_title}"
    
  end

  test "should get treatments" do
    get static_pages_treatments_url
    assert_response :success
    assert_select "title", "treatments | #{@base_title}"
  end

  test "should get news" do
    get static_pages_news_url
    assert_response :success
    assert_select "title", "news | #{@base_title}"
    
  end

  test "should get contactUs" do
    get static_pages_contactUs_url
    assert_response :success
    assert_select "title", "contact us | #{@base_title}"
  end

  test "should get aboutUs" do
    get static_pages_aboutUs_url
    assert_response :success
    assert_select "title", "about us | #{@base_title}"
  end

  test "should get login" do
    get static_pages_login_url
    assert_response :success
    assert_select "title", "login | #{@base_title}"
  end

  test "should get admin" do
    get static_pages_admin_url
    assert_response :success
    assert_select "title", "admin | #{@base_title}"
  end

  test "should get staff" do
    get static_pages_staff_url
    assert_response :success

  end

end

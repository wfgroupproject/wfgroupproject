module StaffLoginsHelper
    # Returns the Gravatar for the given user.
  def gravatar_for(staff_login)
    gravatar_id = Digest::MD5::hexdigest(staff_login.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}"
    image_tag(gravatar_url, alt: staff_login.name, class: "gravatar")
  end
  
  # Logs in the given user.
  def log_in(staff)
    session[:staff_id] = staff.id
  end
  
  # Returns the current logged-in user (if any).
  def current_staff
    @current_staff ||= StaffLogin.find_by(id: session[:staff_id])
  end
  
  # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !current_staff.nil?
  end
  
  def log_out
    session.delete(:staff_id)
    @current_staff = nil
  end
end

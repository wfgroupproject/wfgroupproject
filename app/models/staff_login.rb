class StaffLogin < ApplicationRecord
    validates :staffId, presence: true, length: { maximum: 50 }, uniqueness: { case_sensitive: false }
    validates :name, presence: true, length: { maximum: 50 }
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence: true, length: { maximum: 50 }, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
    validates :address, presence: true, length: { maximum: 255 }
    before_save{ self.email = staffId.downcase }
    before_save { self.staffId = staffId.downcase }
    has_secure_password
    validates :password, presence: true, length: { minimum: 6 }
end

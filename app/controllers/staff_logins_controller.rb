class StaffLoginsController < ApplicationController

  def new
    @staff_login = StaffLogin.new
  end
  
  def show
    @staff_login = StaffLogin.find(params[:id])
  end
  
  def create
    staff = StaffLogin.find_by(email: params[:staff_login][:email].downcase)
    if staff && staff.authenticate(params[:staff_login][:password])
      log_in staff
      redirect_to staff
    else
      # Create an error message.
      flash.now[:danger] = 'Invalid email/password combination'
      redirect_to root_url
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end
  
  private

    def staff_params
      params.require(:staff_login).permit(:staffId, :name, :email, :phone, :role, :accessLevel, :password)
    end
end

class StaticPagesController < ApplicationController
  def home
  end

  def treatments
  end

  def news
  end

  def contactUs
  end

  def aboutUs
  end

  def login
  end

  def admin
  end

  def staff
  end
end

class NewspagesController < ApplicationController
  def contact
  end
  
  def new
    @newspage = Newspage.new
    if @newspage.save
      flash[:success] = "Update the databse"
      redirect_to home_url
    else
      render 'contact'
    end
  end
  
  def create
    @newspage = Newspage.new(newspage_params)
    if @newspage.save
      flash[:success] = "Update the databse"
      redirect_to '/news'
    else
      render 'contact'
       flash[:success] = "failed!"
    end
  end
  
  def show
    @newspage = Newspage.first
  end
  
  def destroy
    Newspage.find(params[:id]).destroy
    redirect_to current_staff
  end
  
  
  def newspage_params
      params.require(:newspage).permit(:text, :string)
  end
  
end

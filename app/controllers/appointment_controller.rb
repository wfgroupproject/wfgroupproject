class MessagesController < ApplicationController
  def contact
  end
  
  def new
    @appointment = Appointment.new
    if @appointment.save
      flash[:success] = "Appointment made!"
      redirect_to home_url
    else
      render 'contact'
    end
  end
  
  def create
    @appointment = Appointment.new(message_params)
    if @appointment.save
      flash[:success] = "Thanks for contacting us!"
      redirect_to home_url
    else
      render 'contact'
       flash[:success] = "failed!"
    end
  end
  
  def show
    @message = Appointment.first
  end
  
  
  def appointment_params
      params.require(:appointment).permit(:appointmentPatientName, :appointmentNotes)
  end
end

class MessagesController < ApplicationController
  def contact
  end
  
  def new
    @message = Message.new
    if @message.save
      flash[:success] = "Thanks for contacting us!"
      redirect_to home_url
    else
      render 'contact'
    end
  end
  
  def create
    @message = Message.new(message_params)
    if @message.save
      flash[:success] = "Thanks for contacting us!"
      redirect_to home_url
    else
      render 'contact'
       flash[:success] = "failed!"
    end
  end
  
  def show
    @message = Message.first
  end
  
  
  def message_params
      params.require(:message).permit(:messageName, :messageEmail, :messageTitle, :messageText, :messagePhone)
  end
end

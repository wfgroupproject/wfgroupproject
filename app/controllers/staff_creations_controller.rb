class StaffCreationsController < ApplicationController
    
    def new
        @staff_creation = StaffLogin.new
    end
  
  def create
    @staff_creation = StaffLogin.new(staff_params)
    if @staff_creation.save!
      flash[:success] = "Staff Member Created!"
      redirect_to root_url
    else
      flash[:danger] = "Staff Member Not Created!"
    end
  end
  
  private

    def staff_params
      params.require(:staff_creation).permit(:staffId, :name, :email, :address, :phone, :role, :accessLevel, :password)
    end
  
end
class CreatePatients < ActiveRecord::Migration[5.1]
  def change
    create_table :patients do |t|
      t.string :patientId
      t.string :string
      t.string :patientName
      t.string :string
      t.string :patientEmail
      t.string :string
      t.string :patientPhone
      t.string :integer
      t.string :patientAllergies
      t.string :string
      t.string :patientMedicalNotes
      t.string :string

      t.timestamps
    end
  end
end

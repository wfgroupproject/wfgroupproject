class AddIndexToStaffId < ActiveRecord::Migration[5.1]
  def change
    add_index :staff_logins, :staffId, unique: true
  end
end

class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.string :messageId
      t.string :string
      t.string :messageTitle
      t.string :string
      t.string :messageText
      t.string :string
      t.string :messageName
      t.string :string
      t.string :messageEmail
      t.string :string
      t.string :messagePhone
      t.string :integer
      t.string :messageReplied
      t.string :boolean
      t.string :messageTime
      t.string :timestamp

      t.timestamps
    end
  end
end

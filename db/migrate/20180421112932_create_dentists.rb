class CreateDentists < ActiveRecord::Migration[5.1]
  def change
    create_table :dentists do |t|
      t.string :dentistName
      t.string :string
      t.string :dentistEmail
      t.string :string
      t.string :dentistPhone
      t.string :integer
      t.string :dentistRegNumber
      t.string :integer
      t.string :dentistIsActive
      t.string :boolean

      t.timestamps
    end
  end
end

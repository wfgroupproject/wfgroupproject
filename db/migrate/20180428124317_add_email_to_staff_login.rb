class AddEmailToStaffLogin < ActiveRecord::Migration[5.1]
  def change
    add_column :staff_logins, :email, :string
  end
end

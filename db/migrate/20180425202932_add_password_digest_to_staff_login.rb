class AddPasswordDigestToStaffLogin < ActiveRecord::Migration[5.1]
  def change
    add_column :staff_logins, :password_digest, :string
  end
end

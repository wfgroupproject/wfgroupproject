class CreateStaffLogins < ActiveRecord::Migration[5.1]
  def change
    create_table :staff_logins do |t|
      t.string :staffId
      t.string :string
      t.string :staffPassword
      t.string :name
      t.string :address
      t.integer :phone
      t.string :role
      t.string :accessLevel
      t.string :integer

      t.timestamps
    end
  end
end

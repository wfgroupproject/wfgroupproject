class AddPasswordToStaffLogins < ActiveRecord::Migration[5.1]
  def change
    add_column :staff_logins, :password, :string
  end
end

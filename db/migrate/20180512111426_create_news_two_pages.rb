class CreateNewsTwoPages < ActiveRecord::Migration[5.1]
  def change
    create_table :news_two_pages do |t|
      t.string :text
      t.string :string
      t.string :textBoxNumber
      t.string :integer

      t.timestamps
    end
  end
end

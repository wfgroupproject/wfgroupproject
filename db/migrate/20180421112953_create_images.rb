class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :images do |t|
      t.string :imageId
      t.string :string
      t.string :imageFileName
      t.string :string
      t.string :imageText
      t.string :string
      t.string :imageCaption

      t.timestamps
    end
  end
end

class RemoveColumns < ActiveRecord::Migration[5.1]
  def change
    remove_column :staff_logins, :string
    remove_column :staff_logins, :staffPassword
    remove_column :staff_logins, :integer
  end
end

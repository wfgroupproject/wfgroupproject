class CreateAppointments < ActiveRecord::Migration[5.1]
  def change
    create_table :appointments do |t|
      t.string :appointmentId
      t.string :string
      t.string :appointmentTimestamp
      t.string :timestamp
      t.string :appointmentPatientName
      t.string :string
      t.string :appointmentDentistId
      t.string :string
      t.string :appointmentLength
      t.string :integer
      t.string :appointmentPrice
      t.string :integer
      t.string :appointmentPaidOrNot
      t.string :boolean
      t.string :appointmentNotes
      t.string :string

      t.timestamps
    end
  end
end

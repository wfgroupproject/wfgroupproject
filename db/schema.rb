# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180513133044) do

  create_table "appointments", force: :cascade do |t|
    t.string "appointmentId"
    t.string "string"
    t.string "appointmentTimestamp"
    t.string "timestamp"
    t.string "appointmentPatientName"
    t.string "appointmentDentistId"
    t.string "appointmentLength"
    t.string "integer"
    t.string "appointmentPrice"
    t.string "appointmentPaidOrNot"
    t.string "boolean"
    t.string "appointmentNotes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dentists", force: :cascade do |t|
    t.string "dentistName"
    t.string "string"
    t.string "dentistEmail"
    t.string "dentistPhone"
    t.string "integer"
    t.string "dentistRegNumber"
    t.string "dentistIsActive"
    t.string "boolean"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "images", force: :cascade do |t|
    t.string "imageId"
    t.string "string"
    t.string "imageFileName"
    t.string "imageText"
    t.string "imageCaption"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.string "messageId"
    t.string "string"
    t.string "messageTitle"
    t.string "messageText"
    t.string "messageName"
    t.string "messageEmail"
    t.string "messagePhone"
    t.string "integer"
    t.string "messageReplied"
    t.string "boolean"
    t.string "messageTime"
    t.string "timestamp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "news_two_pages", force: :cascade do |t|
    t.string "text"
    t.string "string"
    t.string "textBoxNumber"
    t.string "integer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "newsfourpages", force: :cascade do |t|
    t.string "text"
    t.string "string"
    t.string "textBoxNumber"
    t.string "integer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "newspages", force: :cascade do |t|
    t.string "text"
    t.string "string"
    t.string "textBoxNumber"
    t.string "integer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "newsthreepages", force: :cascade do |t|
    t.string "text"
    t.string "string"
    t.string "textBoxNumber"
    t.string "integer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "newstwopages", force: :cascade do |t|
    t.string "text"
    t.string "string"
    t.string "textBoxNumber"
    t.string "integer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "staff_logins", force: :cascade do |t|
    t.string "staffId"
    t.string "name"
    t.string "address"
    t.integer "phone"
    t.string "role"
    t.string "accessLevel"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "email"
    t.string "password"
    t.index ["staffId"], name: "index_staff_logins_on_staffId", unique: true
  end

end
